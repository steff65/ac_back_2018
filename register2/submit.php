<?php 

require_once("dbconfig.php");
$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$facultate = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

//Cerinta a1)
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($cnp) || empty($facebook) || empty($facultate) || empty($birth) || empty($department) || empty($question) || empty($captcha_generated) || empty($captcha_inserted) || empty($check)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

//Cerinta a2)
if(strlen($firstname) > 20 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or Last name is longer than expected!";
}

//Cerinta a2)
if(strlen($firstname) < 3 || strlen($lastname) < 3){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}

//Cerinta a2)
if(strlen($question) < 15){
	$error = 1;
	$error_text = "The answer is shorter than expected!";
}

//Cerinta a3)
if(is_numeric($firstname) || is_numeric($lastname)){
	$error = 1;
	$error_text = "First or Last name is not valid!";
}

//Cerinta a4)
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

//Cerinta a5)
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error = 1;
    $error_text = "Invalid email format"; 
}

//Cerinta a6)
if(strlen($cnp) != 13){
	$error = 1;
	$error_text = "CNP is shorter than expected!";
}
if(($cnp[0]!= '1') && ($cnp[0]!='2') && ($cnp[0]!='3') && ($cnp[0]!='4') && ($cnp[0]!='5') && ($cnp[0]!='6')){
	$error = 1;
	$error_text = "CNP must begin with 1, 2, 3, 4, 5, 6";
}

//Cerinta a7)
if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$facebook)) {
    $error = 1;
    $error_text = "Invalid Facebook URL!";
}

//Cerinta a8)
function birthday($birthday) {

    $birth = date_create($birthday)->diff(date_create('today'))->y;
    
    return $birth;
}

function validateAge($birthday, $age1 = 18, $age2 = 100)
{
    if(is_string($birthday)) {
        $birthday = strtotime($birthday);
    }
    if(time() - $birthday < $age1 * 31536000)  {
        $error = 1;
        $error_text = "You are underage!";
    }
    if(time() - $birthday > $age2 * 31536000){
    	$error = 1;
    	$error_text = "You old piece of shite";
    }
    
}

validateAge($birth, 18, 100);

//Cerinta a9)
if($captcha_inserted != $captcha_generated){
	$error = 1;
	$error_text = "Wrong captcha!";
}


//Cerinta b)
if(is_numeric($facultate)){
	$error = 1;
	$error_text = "Facultatea nu poate contine cifre!";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "Numele facultatii este prea scurt/lung!";
}

//Cerinta c)
/*$con = mysql_connect("register2");
mysql_select_db("register2");

$query1 = "SELECT 1 FROM register2 WHERE `Email` = '$email' LIMIT 1";

$result = mysql_query($query1);

if ( mysql_num_rows ($result) == 1 )
{$value="Mail already registered!";

}
else
{
 mysql_query("INSERT INTO register2 (Email) VALUES ('$email')");
mysql_close($con);

$value="Confirmed";
}
echo $value;
*/

//EXPLICATIE CERINTA C) : am incercat sa gasesc un script care sa nu contina o functie nedefintia anterior, dar nu am gasit decat asta si
//						  nu poate fi verificata unicitatea email-ului fara a face apel la inregistrarile deja existente in baza de date, so here it is.


//Cerinta d)
if($cnp[0]=1 || $cnp[0] = 3 || $cnp[0] = 5){
	$sex = "M";
}
	else{
		$sex = "F";
}

//Cerinta f)
if((substr($cnp,-12,2)!==substr($birth,-8,2)) || (substr($cnp,-10,2)!==substr($birth,-5,2))||(substr($cnp,-8,2)!==substr($birth,-2,2))){
	$error = 1;
	$error_text = "Invalid CNP or date of birth!";
}


try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);
	
//Cerinta e)
	$query = $con->query('SELECT * FROM register2');
	if($query->rowCount()>50){
	$error = 1;
	$error_text = "The maximum number of people has been reached.";

	}

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

if($error == 0) {
	$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,sex,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:sex,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':sex', $sex);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

} else {
    echo "Succes";
}

}

 else {
    echo $error_text;
}


?>